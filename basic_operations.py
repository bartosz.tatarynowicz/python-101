# Basic operations in Python.

# add
print("3 + 7 =",3+7)

# substract
print("7 - 3 =",7-3)

# multiply
print("3 * 7 =",3*7)

# divide
print("3 / 7 =",3/7)

# power (** instead of ^)
print("3 ^ 7 =",3**7)

# modulo
print("7 % 3 =",7%3)

# add strings
print("Hello"+" world")

# multiply strings
print("Hop! "*3)

# conversion to string
print("Hello "+str(555))

# variables
a = 5
b = 6
c = a*b
print("c =",c)

# Sample functions
# BMI calc

# define weight
weight = 115

# define height
height = 1.95

# calculate
bmi = weight / (height**2)

# print result
print("Your BMI is:",bmi)

# Daily Calorie Intake
# define age
age = 31

#define height in cm
heightcm = height * 100

# define sex (-161 for female, +5 for male)
S = 5

# calculate
dci = 10*weight + 6.25 * heightcm - 5*age + S

# print result
print("Your Daily Calorie Intake =",dci)
