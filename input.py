# Some input operations
print("What is your fav movie?")
movie = input()
print("Your favourite movie is",movie)

# input numbers
print("Define a:")
a = input()
print("Define b:")
b = input()
print("a + b =",int(a)+int(b))

# BMI calc with input

# define weight
print("What is your weight?")
weight = float(input())

# define height
print("What is your height in meters?")
height = float(input())

# calculate
bmi = weight / (height**2)

# print result
print("Your BMI is:",bmi)

# Daily Calorie Intake with input
# define age
print("What is your age?")
age = int(input())

#define height in cm
heightcm = float(height) * 100

# define sex (-161 for female, +5 for male)
SM = 5
SF = -161

# calculate
dcim = 10*weight + 6.25 * heightcm - 5*age + SM
dcif = 10*weight + 6.25 * heightcm - 5*age + SF

# print result
print("For female: your Daily Calorie Intake =",dcif)
print("For male: your Daily Calorie Intake =",dcim)
